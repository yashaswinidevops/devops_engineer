provider "aws" {
	  region = "${var.aws_region}"
	  access_key = "${var.aws_access_key}"
	  secret_key = "${var.aws_secret_key}"
	}

data "template_file" "bootstrap_nginx" {
  template = "${file("${path.module}/bootstrap/bootstrap_nginx.tpl")}"
}

data "template_file" "bootstrap_hopper" {
  template = "${file("${path.module}/bootstrap/bootstrap_hopper.tpl")}"
}

module "key_pair_bastion" {
  source          = "./terraform_modules/key_pair"
  public_key_path = "${var.pub_bastionkey_path}"
  name            = "bastion-nfc"
}



module "nfc-hopper" {
  source              = "./terraform_modules/ec2"
  type                = "t2.micro"
  key_pair_id         = "bastion-nfc"
  aws_ami_map         = "${var.default_ami}"
  aws_region          = "${var.aws_region}"
  aws_region_os       = "${var.aws_region_os}"
  subnet_id           = "${var.public_subnet_1}"
  security_group_ids  = ["${aws_security_group.task_sharedsvc_sg.id}"]
  user_data           = "${data.template_file.bootstrap_hopper.rendered}"
  root_volume_size    = "${var.root_volume_size_nginxrouter}"
  name                = "task-prod-hopper"
  env                 = "prod"
  project             = "nfc"
  class               = "hopper"
  purpose             = "router"
}



module "asg_task_nginx" {
  source              = "./terraform_modules/ec2_asg"
  name                = "asg_task_nginx"
  instance_subnets    = ["${var.private_subnet_1}", "${var.private_subnet_2}"]
  asg_max             = "${var.nginx_asg_max}"
  asg_min             = "${var.nginx_asg_min}"
  asg_desired         = "${var.nginx_asg_desired}"
  ami_id              = "${var.nginx_ami}"
  instance_type       = "t2.medium"
  security_group_ids  = [ "${aws_security_group.task_pub_web_sg.id}" , "${aws_security_group.task_sharedsvc_sg.id}"]
  user_data           = "${data.template_file.bootstrap_nginx.rendered}"
  asg_root_volume_size  = "${var.root_volume_size}"
  load_balancer = "${aws_elb.task-nginx-pub-elb.id}"
  env           =  "task"
  project       =  "task"
  class         =  "nginx"
  purpose       =  "router"
}
