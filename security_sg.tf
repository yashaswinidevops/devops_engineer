resource "aws_security_group" "task_pub_web_sg" {
  name   = "task_pub_web_sg"
  vpc_id = "${var.vpc_id}"
#   vpc_id = "vpc-3da6805b"
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "from internet"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "from internet"
  }

  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }

  tags {
    Name = "task_pub_web_sg"
    }
}


resource "aws_security_group" "task_sharedsvc_sg" {
  name   = "task_sharedsvc_sg"
  vpc_id = "${var.vpc_id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    self        = "true"
    cidr_blocks = ["192.168.0.96/28"]
    description = "from shared svc vpc"
  }

  egress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["192.168.0.96/28"]
    }

  tags {
    Name = "task_sharedsvc_sg"
    }
}


resource "aws_security_group" "task-elb-elb" {
  name   = "task_elb_sg"
  vpc_id = "${var.vpc_id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    self        = "true"
    description = "self rule"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    self        = "true"
    description = "self rule"
  }

  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }

  tags {
    Name = "task_elb_sg"
    }
}








 resource "aws_security_group" "task_pub_ssh_sg" {
   name   = "task_pub_ssh_sg"
   vpc_id = "${var.vpc_id}"

   ingress {
     from_port   = 22
     to_port     = 22
     protocol    = "tcp"
     cidr_blocks = ["0.0.0.0/0"]
     description = "from internet"
   }

   egress {
       from_port = 0
       to_port = 0
       protocol = "-1"
       cidr_blocks = ["0.0.0.0/0"]
     }

   tags {
     Name = "task_pub_ssh_sg"
     }
 }

