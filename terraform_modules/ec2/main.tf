resource "aws_instance" "instance" {
  ami                    = "${lookup(var.aws_ami_map, var.aws_region_os)}"
  count                  = "${var.number_of_instances}"
  subnet_id              = "${var.subnet_id}"
  instance_type          = "${var.type}"
  key_name               = "${var.key_pair_id}"
  iam_instance_profile   = "${var.iam_instance_profile}"
  vpc_security_group_ids = ["${var.security_group_ids}"]
  user_data              = "${var.user_data}"
  #associate_public_ip_address              = "${var.public_ip_association}"

  root_block_device {
    volume_type           = "${var.root_volume_type}"
    volume_size           = "${var.root_volume_size}"
    delete_on_termination = "true"
  }

  tags {
    Name = "${var.name}"
  }

  tags {
    env = "${var.env}"
  }

  tags {
    project = "${var.project}"
  }

  tags {
    class = "${var.class}"
  }

  tags {
    purpose = "${var.purpose}"
  }

  tags {
    managedby = "${var.managedby}"
  }

}

