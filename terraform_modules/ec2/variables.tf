variable "name" {}

variable "env" {}

variable "project" {}

variable "class" {}

variable "purpose" {}

#variable "public_ip_association" {}

variable "managedby" {
  default = "terraform"
}

variable "type" {
  default = "t2.micro"
}

variable "subnet_id" {}

variable "number_of_instances" {
  default = 1
}

variable "key_pair_id" {
  default = ""
}

variable "iam_instance_profile" {
  default = ""
}

variable "aws_ami_map" {
  type = "map"

  default = {
    ap-south-1-centos            = "ami-95cda6fa"
    ap-south-1-ubuntu            = "ami-83a8dbec"
    ap-southeast-1-centos        = "ami-f068a193"
    ap-southeast-1-windows       = "ami-db1ea6b8"
    ap-southeast-1-centosdeepak  = "ami-9114acf2"
    ap-southeast-1-windowsdeepak = "ami-0515ad66"
    ap-southeast-1-ubuntu        = "ami-f068a193"
    eu-west-1-centos             = "ami-7abd0209"
    us-west-2-centos             = "ami-14b07274"
    us-west-2-ubuntu		 = "ami-d94f5aa0"
    us-east-1-ubuntu             = "ami-772aa961"
    us-east-1-centos             = "ami-366be821"
    us-west-1-centos             = "ami-e4c78f84" 
 }
}

variable "aws_region" {}

variable "aws_region_os" {}

variable "chef_tags" {
  default = "aurora"
}

variable "root_volume_type" {
  default = "gp2"
}

variable "root_volume_size" {
  default = "30"
}

variable "security_group_ids" {
  type = "list"
}

#variable "zone_id" {}

variable "user_data" {
  default = ""
}
