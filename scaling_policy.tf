#
# ########### app
#
# #scale up
#
 resource "aws_autoscaling_policy" "scale-up-policy-nginx" {
   name                   = "scale-up-policy-nginx"
   scaling_adjustment     = 1
   adjustment_type        = "ChangeInCapacity"
   cooldown               = 150
   autoscaling_group_name = "${module.asg_task_nginx.asg_name}"
}

 resource "aws_cloudwatch_metric_alarm" "cpualarm-up-nginx" {
   alarm_name          = "cpualarm-up-nginx"
   comparison_operator = "GreaterThanOrEqualToThreshold"
   evaluation_periods  = "1"
   metric_name         = "CPUUtilization"
   namespace           = "AWS/EC2"
   period              = "120"
   statistic           = "Average"
   threshold           = "70"
   dimensions {
     AutoScalingGroupName = "${module.asg_task_nginx.asg_name}"
   }
   alarm_description = "This metric monitors ec2 cpu utilization"
   alarm_actions     = ["${aws_autoscaling_policy.scale-up-policy-nginx.arn}"]
}

 # scale down

 resource "aws_autoscaling_policy" "scale-down-policy-nginx" {
   name                   = "scale-down-policy-nginx"
   scaling_adjustment     = -1
   adjustment_type        = "ChangeInCapacity"
   cooldown               = 300
   autoscaling_group_name = "${module.asg_task_nginx.asg_name}"
 }

 resource "aws_cloudwatch_metric_alarm" "cpualarm-down-nginx" {
   alarm_name          = "cpualarm-down-nginx"
   comparison_operator = "LessThanOrEqualToThreshold"
   evaluation_periods  = "1"
   metric_name         = "CPUUtilization"
   namespace           = "AWS/EC2"
   period              = "120"
   statistic           = "Average"
   threshold           = "40"
   dimensions {
     AutoScalingGroupName = "${module.asg_task_nginx.asg_name}"
   }
   alarm_description = "This metric monitors ec2 cpu utilization"
   alarm_actions     = ["${aws_autoscaling_policy.scale-down-policy-nginx.arn}"]
 }



