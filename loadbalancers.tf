resource "aws_elb" "task-nginx-pub-elb" {
  name               = "task-nginx-pub-elb"
  subnets            = ["${var.public_subnet_1}", "${var.public_subnet_2}"]
  security_groups    = [ "${aws_security_group.task_pub_web_sg.id}"]

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "TCP:80"
    interval            = 30
  }

   listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
    }

#  listener {
#    instance_port     = 80
#    instance_protocol = "http"
#    lb_port           = 443
#    lb_protocol       = "https"
#    }

   tags {
     Name = "task-nginx-pub-elb"
   }

}
